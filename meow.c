#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>

#define DEVICE_NAME "meow"

static int dev_open(struct inode*, struct file*);
static int dev_release(struct inode*, struct file*);
static ssize_t dev_read(struct file*, char*, size_t, loff_t*);
static ssize_t dev_write(struct file*, const char*, size_t, loff_t*);

static struct file_operations fops = {
    .open = dev_open,
    .read = dev_read,
    .write = dev_write,
    .release = dev_release,
};

static int major;

static int __init meow_init(void) {
    major = register_chrdev(0, DEVICE_NAME, &fops);

    if (major < 0) {
        printk(KERN_ALERT "meow won't load\n");
        return major;
    }

    printk(KERN_INFO "meow: %i\n", major);
    return 0;
};

static void __exit meow_exit(void) {
    unregister_chrdev(major, DEVICE_NAME);
    printk(KERN_INFO "woem\n");
}

static int dev_open(struct inode *inodep, struct file *filep) {
    printk(KERN_INFO "meow dev open\n");
    return 0;
}

static ssize_t dev_write(struct file *filep, const char *buffer,
                         size_t len, loff_t *offset) {
    printk(KERN_INFO "can nyat write uwu\n");
    return -EFAULT;
}

static int dev_release(struct inode *inodep, struct file *filep) {
    printk(KERN_INFO "woem dev close\n");
    return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
    int errors = 0;
    char *message = "meow \n";
    int message_len = strlen(message);

    errors = copy_to_user(buffer, message, message_len);

    return errors == 0 ? message_len : -EFAULT;
}


module_init(meow_init);
module_exit(meow_exit);
MODULE_LICENSE("GPL");
