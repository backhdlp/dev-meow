# /dev/meow
Linux kernel module that adds /dev/meow as a character device that outputs meow.

# Important info I think
99% of the current code is stolen from this tutorial I followed pretty much word by word:
https://www.youtube.com/watch?v=CWihl19mJig

# Install
- clone this repo and cd into it
```bash
git clone https://gitlab.com/backhdlp/dev-meow.git && cd dev-meow
```
- Build the module:
```bash
make
```
- load the module
```bash
sudo insmod meow.ko
```
- check dmesg for a message like this (with <number> being an actual number)
```
meow: <number>
```
- create the filesystem node, \<number> here should be same as the number you got from dmesg
```bash
sudo mknod /dev/meow c <number> 0
```
- you can now `cat` the file to spam your terminal with meows

# Uninstall
- unload the module
```bash
sudo rmmod meow
```
- remove the filesystem node
```bash
sudo rm /dev/meow
```
- instead of the previous steps, you can also just reboot

- (optional) clean the repository from all the build files (this will remove the .ko file too)
```bash
make clean
```
- (optional) if you don't want to keep the repo, you can just delete it 
```bash
cd .. && rm -r dev-meow 
```

# TODO
- /dev/woem ? 
- only one meow maybe
- move Makefile output to it's own directory so I can .gitignore it
- hope everything still works after a reboot or else this is gonna be a bit awkward.
